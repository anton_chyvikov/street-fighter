import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  // todo: show fighter info (image, name, health, etc.) !Done

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter !== undefined) {
    const { name, health, attack, defense } = fighter;

    const fighterDescription = createElement({
      tagName: 'div',
      className: 'fighter-preview___description',
    });

    const fighterName = createElement({
      tagName: 'h3',
      className: 'fighter-preview___title',
    });
    fighterName.innerHTML = name;
    fighterDescription.append(fighterName);

    const fighterHealth = createElement({
      tagName: 'p',
      className: 'fighter-preview___property',
    });
    fighterHealth.innerHTML = `Health: ${health}`;
    fighterDescription.append(fighterHealth);

    const fighterAttack = createElement({
      tagName: 'p',
      className: 'fighter-preview___property',
    });
    fighterAttack.innerHTML = `Attack: ${attack}`;
    fighterDescription.append(fighterAttack);

    const fighterDefence = createElement({
      tagName: 'p',
      className: 'fighter-preview___property',
    });
    fighterDefence.innerHTML = `Defense: ${defense}`;
    fighterDescription.append(fighterDefence);

    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(fighterDescription);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
