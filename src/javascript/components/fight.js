import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over !Done

    const leftFighterHealthIndicator = document.querySelector('#left-fighter-indicator');
    leftFighterHealthIndicator.style.width = '100%';
    const rightFighterHealthIndicator = document.querySelector('#right-fighter-indicator');
    rightFighterHealthIndicator.style.width = '100%';

    //Left fighter
    let leftFighterHealthCounter = +firstFighter.health;
    let isLeftFighterInBlock = false;
    let isItFirstKeyDownOfLeftFighterHit = true;
    const leftFighterSuperHitFlags = {
      isKeyDownQ: false,
      isItFirstKeyDownQ: true,
      isKeyDownW: false,
      isItFirstKeyDownW: true,
      isKeyDownE: false,
      isItFirstKeyDownE: true,
      isSuperHitAvailable: true,
    };

    //Right fighter
    let rightFighterHealthCounter = +secondFighter.health;
    let isRightFighterInBlock = false;
    let isItFirstKeyPressOfRightFighterHit = true;
    const rightFighterSuperHitFlags = {
      isKeyDownU: false,
      isItFirstKeyDownU: true,
      isKeyDownI: false,
      isItFirstKeyDownI: true,
      isKeyDownO: false,
      isItFirstKeyDownO: true,
      isSuperHitAvailable: true,
    };

    function updateLeftFighterHealthIndicator() {
      leftFighterHealthIndicator.style.width =
        getHealthInPercents(+firstFighter.health, leftFighterHealthCounter) + '%';

      if (leftFighterHealthCounter < 0) {
        leftFighterHealthIndicator.style.width = 0;
      }
    }

    function isLeftFighterWin() {
      if (rightFighterHealthCounter < 0) return true;
    }

    function updateRightFighterHealthIndicator() {
      rightFighterHealthIndicator.style.width =
        getHealthInPercents(+secondFighter.health, rightFighterHealthCounter) + '%';

      if (rightFighterHealthCounter < 0) {
        rightFighterHealthIndicator.style.width = 0;
      }
    }

    function isRightFighterWin() {
      if (leftFighterHealthCounter < 0) return true;
    }

    //=============================Keydown listener===================================
    document.addEventListener('keydown', (e) => {
      // Left fighter attack
      if (e.code === controls['PlayerOneAttack'] && isItFirstKeyDownOfLeftFighterHit && !isLeftFighterInBlock) {
        isItFirstKeyDownOfLeftFighterHit = false;

        if (!isRightFighterInBlock) {
          rightFighterHealthCounter -= getDamage(firstFighter, secondFighter);
        }

        updateRightFighterHealthIndicator();
        if (isLeftFighterWin()) resolve(firstFighter);
      }

      // Left fighter block
      if (e.code === controls['PlayerOneBlock']) {
        isLeftFighterInBlock = true;
      }

      // Left fighter super attack
      if (e.code === controls['PlayerOneCriticalHitCombination'][0] && leftFighterSuperHitFlags['isItFirstKeyDownQ']) {
        leftFighterSuperHitFlags['isKeyDownQ'] = true;
        leftFighterSuperHitFlags['isItFirstKeyDownQ'] = false;

        if (
          !isLeftFighterInBlock &&
          leftFighterSuperHitFlags['isKeyDownW'] &&
          leftFighterSuperHitFlags['isKeyDownE'] &&
          leftFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          rightFighterHealthCounter -= getSuperHitPower(firstFighter);

          updateRightFighterHealthIndicator();
          if (isLeftFighterWin()) resolve(firstFighter);

          leftFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(leftFighterSuperHitFlags);
        }
      }

      if (e.code === controls['PlayerOneCriticalHitCombination'][1] && leftFighterSuperHitFlags['isItFirstKeyDownW']) {
        leftFighterSuperHitFlags['isKeyDownW'] = true;
        leftFighterSuperHitFlags['isItFirstKeyDownW'] = false;

        if (
          !isLeftFighterInBlock &&
          leftFighterSuperHitFlags['isKeyDownQ'] &&
          leftFighterSuperHitFlags['isKeyDownE'] &&
          leftFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          rightFighterHealthCounter -= getSuperHitPower(firstFighter);

          updateRightFighterHealthIndicator();
          if (isLeftFighterWin()) resolve(firstFighter);

          leftFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(leftFighterSuperHitFlags);
        }
      }

      if (e.code === controls['PlayerOneCriticalHitCombination'][2] && leftFighterSuperHitFlags['isItFirstKeyDownE']) {
        leftFighterSuperHitFlags['isKeyDownE'] = true;
        leftFighterSuperHitFlags['isItFirstKeyDownE'] = false;

        if (
          !isLeftFighterInBlock &&
          leftFighterSuperHitFlags['isKeyDownQ'] &&
          leftFighterSuperHitFlags['isKeyDownW'] &&
          leftFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          rightFighterHealthCounter -= getSuperHitPower(firstFighter);

          updateRightFighterHealthIndicator();
          if (isLeftFighterWin()) resolve(firstFighter);

          leftFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(leftFighterSuperHitFlags);
        }
      }

      // Right fighter attack
      if (e.code === controls['PlayerTwoAttack'] && isItFirstKeyPressOfRightFighterHit && !isRightFighterInBlock) {
        isItFirstKeyPressOfRightFighterHit = false;

        if (!isLeftFighterInBlock) {
          leftFighterHealthCounter -= getDamage(secondFighter, firstFighter);
        }

        updateLeftFighterHealthIndicator();
        if (isRightFighterWin()) resolve(secondFighter);
      }

      // Right fighter block
      if (e.code === controls['PlayerTwoBlock']) {
        isRightFighterInBlock = true;
      }

      // Right fighter super attack
      if (e.code === controls['PlayerTwoCriticalHitCombination'][0] && rightFighterSuperHitFlags['isItFirstKeyDownU']) {
        rightFighterSuperHitFlags['isKeyDownU'] = true;
        rightFighterSuperHitFlags['isItFirstKeyDownU'] = false;

        if (
          !isRightFighterInBlock &&
          rightFighterSuperHitFlags['isKeyDownI'] &&
          rightFighterSuperHitFlags['isKeyDownO'] &&
          rightFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          leftFighterHealthCounter -= getSuperHitPower(secondFighter);

          updateLeftFighterHealthIndicator();
          if (isRightFighterWin()) resolve(secondFighter);

          rightFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(rightFighterSuperHitFlags);
        }
      }

      if (e.code === controls['PlayerTwoCriticalHitCombination'][1] && rightFighterSuperHitFlags['isItFirstKeyDownI']) {
        rightFighterSuperHitFlags['isKeyDownI'] = true;
        rightFighterSuperHitFlags['isItFirstKeyDownI'] = false;

        if (
          !isRightFighterInBlock &&
          rightFighterSuperHitFlags['isKeyDownU'] &&
          rightFighterSuperHitFlags['isKeyDownO'] &&
          rightFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          leftFighterHealthCounter -= getSuperHitPower(secondFighter);

          updateLeftFighterHealthIndicator();
          if (isRightFighterWin()) resolve(secondFighter);

          rightFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(rightFighterSuperHitFlags);
        }
      }

      if (e.code === controls['PlayerTwoCriticalHitCombination'][2] && rightFighterSuperHitFlags['isItFirstKeyDownO']) {
        rightFighterSuperHitFlags['isKeyDownO'] = true;
        rightFighterSuperHitFlags['isItFirstKeyDownO'] = false;

        if (
          !isRightFighterInBlock &&
          rightFighterSuperHitFlags['isKeyDownU'] &&
          rightFighterSuperHitFlags['isKeyDownI'] &&
          rightFighterSuperHitFlags['isSuperHitAvailable']
        ) {
          leftFighterHealthCounter -= getSuperHitPower(secondFighter);

          updateLeftFighterHealthIndicator();
          if (isRightFighterWin()) resolve(secondFighter);

          rightFighterSuperHitFlags['isSuperHitAvailable'] = false;
          makeSuperHitAvailableAfterDelay(rightFighterSuperHitFlags);
        }
      }
    });

    //=============================Keyup listener=====================================
    document.addEventListener('keyup', (e) => {
      // Left fighter attack
      if (e.code === controls['PlayerOneAttack']) {
        isItFirstKeyDownOfLeftFighterHit = true;
      }

      // Left fighter block
      if (e.code === controls['PlayerOneBlock']) {
        isLeftFighterInBlock = false;
      }

      // Right fighter attack
      if (e.code === controls['PlayerTwoAttack']) {
        isItFirstKeyPressOfRightFighterHit = true;
      }

      // Right fighter block
      if (e.code === controls['PlayerTwoBlock']) {
        isRightFighterInBlock = false;
      }

      // Left fighter super attack
      if (e.code === controls['PlayerOneCriticalHitCombination'][0]) {
        leftFighterSuperHitFlags['isKeyDownQ'] = false;
        leftFighterSuperHitFlags['isItFirstKeyDownQ'] = true;
      }

      if (e.code === controls['PlayerOneCriticalHitCombination'][1]) {
        leftFighterSuperHitFlags['isKeyDownW'] = false;
        leftFighterSuperHitFlags['isItFirstKeyDownW'] = true;
      }

      if (e.code === controls['PlayerOneCriticalHitCombination'][2]) {
        leftFighterSuperHitFlags['isKeyDownE'] = false;
        leftFighterSuperHitFlags['isItFirstKeyDownE'] = true;
      }

      // Right fighter super attack
      if (e.code === controls['PlayerTwoCriticalHitCombination'][0]) {
        rightFighterSuperHitFlags['isKeyDownU'] = false;
        rightFighterSuperHitFlags['isItFirstKeyDownU'] = true;
      }

      if (e.code === controls['PlayerTwoCriticalHitCombination'][1]) {
        rightFighterSuperHitFlags['isKeyDownI'] = false;
        rightFighterSuperHitFlags['isItFirstKeyDownI'] = true;
      }

      if (e.code === controls['PlayerTwoCriticalHitCombination'][2]) {
        rightFighterSuperHitFlags['isKeyDownO'] = false;
        rightFighterSuperHitFlags['isItFirstKeyDownO'] = true;
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = getRandomArbitrary(1, 2);
  const hitPower = +fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandomArbitrary(1, 2);
  const blockPower = +fighter.defense * dodgeChance;
  return blockPower;
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function getHealthInPercents(startHealth, currentHealth) {
  const startHealthInPercents = 100;
  return Math.floor(currentHealth / (startHealth / startHealthInPercents));
}

function getSuperHitPower(fighter) {
  const superHitPower = +fighter.attack * 2;
  return superHitPower;
}

function makeSuperHitAvailableAfterDelay(fighterSuperHitFlags) {
  let delay = setTimeout(() => {
    fighterSuperHitFlags.isSuperHitAvailable = true;
    clearTimeout(delay);
  }, 10000);
}
