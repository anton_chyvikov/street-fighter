import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter) {
  // call showModal function !Done

  const { name } = fighter;
  function newGame() {
    const rootElement = document.getElementById('root');
    rootElement.innerHTML = '';
    new App();
  }

  const contentForModal = {
    title: `Congratulations, ${name}!`,
    bodyElement: createFighterImage(fighter),
    onClose: newGame,
  };

  showModal(contentForModal);
}
